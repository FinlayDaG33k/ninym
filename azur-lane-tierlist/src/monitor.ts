import puppeteer from "https://deno.land/x/puppeteer@9.0.1/mod.ts";
import { Logger } from "../../lib/logging/logger.ts";
import { cron } from "../../lib/common/cron.ts";

export class Monitor {
  private page = 'https://slaimuda.github.io/ectl/#/home';

  constructor() {
    this.scrape();
  }

  private async scrape() {
    // Init new browser
    Logger.debug('Starting puppeteer');
    const browser = await puppeteer.launch({
      headless: false,
      args: [
        '--window-size=1920,1080',
      ],
    });
    const page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 1080 });

    // Go to page to scrape
    await page.goto(this.page, {
      waitUntil: "networkidle0",
    });

    // Wait for 1 second
    await page.waitFor(1000);

    // Get a list of all ship types
    // Then click them
    Logger.debug('Clicking all types');
    const types = await page.$$('ul.nav-tabs[role="tablist"] li.nav-item');
    for(let i = 1; i < types.length; i++) {
      await page.waitFor(500);
      await types[i].click();
    }

    // Get the data for all types
    Logger.debug('Obtaining tierlist for each type');
    const tabs = await page.$$('div.card-body div.tab-pane');
    console.log(tabs.length);
    for(let i = 1; i < tabs.length; i++) {
      const html = await page.evaluate(el => el.innerHTML, tabs[i]);
      console.log(html);
    }

    // await browser.close();
    console.log('done');
  }
}
