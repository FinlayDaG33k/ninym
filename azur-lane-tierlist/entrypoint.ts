import { config as env } from "https://deno.land/x/dotenv/mod.ts";
import { Monitor } from "./src/monitor.ts";
import { Bootstrap } from "../lib/communication/bootstrap.ts";

// Load our env file
/*
const bootstrap = await Bootstrap.getEnv('template');
if(!bootstrap) Deno.exit(1);

// Load env
env({
  path: './.env',
  export: true
});
*/
// Start the monitor
const monitor = new Monitor();
