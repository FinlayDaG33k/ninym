import { Configure } from "./deps.ts";
import { QueueEngine } from "./src/queue-engine.ts";

// Load configuration
await Configure.load();

// Start QueueEngine
const engine = new QueueEngine();
