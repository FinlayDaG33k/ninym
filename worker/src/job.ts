import { QueueEngine } from "./queue-engine.ts";

export class Job {
  protected _errors: string[] = [];
  public get errors(): string[] { return this._errors; }
  public addError(error: string) { this._errors.push(error); }

  constructor(
    protected readonly id: string,
    protected readonly opts: any = {},
  ) {
  }

  /**
   * Fetch something from an external location.
   * Works the same as a vanilla "fetch" except this automatically times out.
   * 
   * @param input
   * @param init
   * @param timeout
   */
  protected fetch(input: URL|Request|string, init: RequestInit = {}, timeout = 5000): Promise<Response> {
    // Inject automatic abortion after 5 seconds
    const controller = new AbortController();
    init.signal = controller.signal;
    setTimeout(() => controller.abort(), timeout);
    
    // Create and return fetch
    return fetch(input, init);
  }
}
