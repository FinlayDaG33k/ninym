import { Logger, Configure, Cron } from "../deps.ts";

interface JobData {
  id: string;
  job: string;
  opts: any;
}

export class QueueEngine {
  private isWorking = false;
  constructor() {
    Cron('*/5 * * * * *', async () => {
      if(this.isWorking) return;
      await this.execute();
    });
  }

  private async execute() {
    // Make sure we aren't double-checking
    if(this.isWorking) return;
    this.isWorking = true;

    // Check if a job is available
    const hasJob = await QueueEngine.check();
    if(!hasJob) {
      Logger.debug('No job found on master!');
      this.isWorking = false;
      return;
    }
    Logger.debug('Master has job, getting it!');

    // Get job
    const job = await QueueEngine.get();

    // Run job
    Logger.debug(`Running job "${job.job}" (${job.id})!`);
    await QueueEngine.work(job);
    Logger.debug(`Finished job "${job.job}" (${job.id})!`);

    // Mark as finished
    this.isWorking = false;
  }

  private static async check() {
    let body;
    try {
      const resp = await fetch(`${Configure.get('master_host', 'http://localhost:8080')}/queue/check`);
      body = await resp.text();
    } catch(e) {
      Logger.error(`Could not check master for work: "${e.message}"`, e.stack);
      return false;
    }

    return body == 'true';
  }

  private static async get() {
    try {
      const resp = await fetch(`${Configure.get('master_host', 'http://localhost:8080')}/queue/get`, {
        headers: {
          'Authorization': `Bearer ${Configure.get('auth_secret', '')}`
        }
      });
      return await resp.json();
    } catch(e) {
      Logger.error(`Could obtain job from master: "${e.message}"`, e.stack);
      return null;
    }
  }

  private static async work(job: JobData) {
    // Import the job handler
    let imported;
    try {
      imported = await import(`file://${Deno.cwd()}/src/job/${job.job}.job.ts`);
    } catch(e) {
      await QueueEngine.report(job.id, e.message);
      return;
    }

    // Get the class associated with the job
    const tokens = [];
    for(let token of job.job.split('-')) {
      token = token.toLowerCase();
      token = token[0].toUpperCase() + token.slice(1);
      tokens.push(token);
    }
    const name = `${tokens.join('')}Job`;

    // Instantiate the job handler
    let handler;
    try {
      handler = new imported[name](job.id, job.opts);
    } catch(e) {
      await QueueEngine.report(job.id, e.message);
      return;
    }

    // Run the job
    try {
      await handler['execute']();
    } catch(e) {
      Logger.error(`Could not run job "${job.job}" (${job.id}): "${e.message}"`, e.stack);
      handler.addError(e.message);
    }

    // Report the job
    try {
      await QueueEngine.report(job.id, handler.errors)
    } catch(e) {
      Logger.error(`Could not report job "${job.job}" (${job.id}): "${e.message}"`, e.stack);
    }
  }

  public static async report(id: string, error: string[]) {
    try {
      await fetch(`${Configure.get('master_host', 'http://localhost:8080')}/queue/report`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${Configure.get('auth_secret', '')}`
        },
        body: JSON.stringify({ id: id, error: error })
      });
    } catch(e) {
      Logger.error(`Could not report to master: "${e.message}"`, e.stack);
    }
  }
}
