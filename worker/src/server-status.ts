import { Configure } from "../deps.ts";

export class ServerStatuses {
  public static async update(service: string, status: string) {
    const resp = await fetch(`${Configure.get('master_host', 'http://localhost:8080')}/status/${service}`, {
      method: 'PATCH',
      headers: {
        authorization: `Bearer ${Configure.get('auth_secret')}`,
      },
      body: status
    });

    // Get the content from the response
    const content = await resp.text();
    if(content !== 'OK') throw Error(content);
  }
}
