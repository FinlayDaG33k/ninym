import { Job } from "../job.ts";
import {Database} from "../database.ts";

interface SeriesInfo {
  id: number;
  name: string;
  rating: number|string;
  progress: string;
  status: number|string;
}

enum SeriesStatus {
  UNKNOWN,
  NOT_STARTED,
  DROPPED,
  COMPLETED,
  BACKLOGGED,
  WATCHING,
}

export class AnilistJob extends Job {
  private list: SeriesInfo[] = [];
  private apiKey: string = '';
  private static readonly pageSize = 50;
  private backlogTarget: number;
  private droppedTarget: number;
  
  public async execute() {
    // Prepare backlog and dropped targets
    const dropped = new Date();
    const backlog = new Date();
    dropped.setDate(dropped.getDate() - 90);
    backlog.setDate(backlog.getDate() - 30);
    this.droppedTarget = dropped.getTime();
    this.backlogTarget = backlog.getTime();
    
    // Authorize with API
    await this.auth();
    if(this.apiKey === '') return;
    
    // Get series info
    const res = await this.getList();
    
    // Only allow storing new version if everything went alright
    if(!res) return;

    // Sort our list by conditions:
    // 1. Status (see AnidbJob#parseStatus)
    // 2. Rating
    // 3. Alphabetical
    this.list.sort((a: SeriesInfo, b: SeriesInfo) =>
      (b.status as number) - (a.status as number) ||
      (b.rating as number) - (a.rating as number) ||
      a.name.localeCompare(b.name)
    );

    // Replace statuses and TBD ratings
    this.list = this.list.map((entry: SeriesInfo) => {
      switch(entry.status) {
        case SeriesStatus.NOT_STARTED:
          entry.status = 'Not Started';
          break;
        case SeriesStatus.DROPPED:
          entry.status = 'Dropped';
          break;
        case SeriesStatus.BACKLOGGED:
          entry.status = 'Backlogged';
          break;
        case SeriesStatus.COMPLETED:
          entry.status = 'Completed';
          break;
        case SeriesStatus.WATCHING:
          entry.status = 'Watching';
          break;
        case SeriesStatus.UNKNOWN:
        default:
          entry.status = 'Unknown';
      }
      
      entry.rating = this.rating(entry.rating);

      return entry;
    });
    
    // Store in database
    try {
      await Database.set('anidb-list', this.list);
    } catch(e) {
      this.addError(`Could not set database key: ${e.message}`);
    }
  }

  /**
   * Authenticate against Shoko
   * 
   * @returns Promise<string|null> API key or null
   */
  private async auth(): Promise<void> {
    try {
      // TODO: Replace with opts from master
      const res = await this.fetch(`${this.opts['shoko_host']}/api/auth`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          user: this.opts['shoko_user'],
          pass: this.opts['shoko_pass'],
          device: "ninym",
          rememberUser:false,
        }),
      });
      if(res.status !== 200) throw new Error(`${res.status} - ${res.statusText}`);
      const data = await res.json();
      this.apiKey = data['apikey'] ?? '';
    } catch(e) {
      this.addError(`Could not authorize with Shoko: "${e.message}"`);
    }
  }
  
  private async getList(): Promise<boolean> {
    let page = 1;
    while(true) {
      let data;
      try {
        const res = await this.fetch(`${this.opts['shoko_host']}/api/v3/Series?page=${page}`, {
          method: 'GET',
          headers: {
            apiKey: this.apiKey,
          }
        });
        if(res.status !== 200) throw new Error(`${res.status} - ${res.statusText}`);
        data = await res.json();
      } catch(e) {
        this.addError(`Could not get page #${page} from Shoko: "${e.message}"`);
        return false;
      }

      // Loop over each entry and add it to our list
      for(const entry of data['List']) {
        this.list.push({
          id: entry['IDs']['AniDB'],
          name: entry['Name'],
          rating: entry['UserRating'] ? entry['UserRating']['Value'] : -1,
          progress: `${entry['Sizes']['Watched']['Episodes']}/${entry['Sizes']['Total']['Episodes']}`,
          status: await this.status(entry),
        });
      }
      
      // Break the loop if we had less results than the page size
      // Otherwise, increase page and keep going
      if(data['List'].length < AnilistJob.pageSize) break;
      page++
    }
    
    // We didn't encounter any errors
    return true;
  }

  private rating(rating: number): string {
    switch(rating) {
      case 8.5:
      case 8:
        return "Masterpiece";
      case 7.5:
      case 7:
      case 6.5:
        return "Yes";
      case 6:
        return "Niche";
      case -1:
        return "TBD";
    }
    return "No";
  }
  
  private async status(entry: any): Promise<SeriesStatus> {
    // Check if we watched all episodes
    if(entry['Sizes']['Watched']['Episodes'] === entry['Sizes']['Total']['Episodes']) return SeriesStatus.COMPLETED;
    
    // Get the timestamp of last update
    let lastWatched: number;
    try {
      const res = await this.fetch(`${this.opts['shoko_host']}/api/v3/series/${entry['IDs']['ID']}/Episode`, {
        headers: {
          apiKey: this.apiKey, 
        },
      });
      if(res.status !== 200) throw new Error(`${res.status} - ${res.statusText}`);
      const data = await res.json();

      // Make sure we have episodes
      if(data['List'].length === 0) return SeriesStatus.UNKNOWN;
      
      // Get the date of the last watched episode
      lastWatched = new Date(Math.max(...data['List'].map(e => new Date(e['Watched'])))).getTime();
    } catch(e) {
      this.addError(`Could not get status for "${entry['Name']}": "${e.message}"`);
      return;
    }
    
    // Check if we've even started
    if(lastWatched === 0) return SeriesStatus.NOT_STARTED;
    
    // Check if we have updated it in the last two weeks (watching)
    if(lastWatched >= this.backlogTarget) return SeriesStatus.WATCHING;
    
    // Check if we have updated in the last month (backlogged)
    if(lastWatched >= this.droppedTarget) return SeriesStatus.BACKLOGGED;
    
    // We haven't updated in a month, probably dropped it.
    return SeriesStatus.DROPPED;
  }
}
