import { Job } from "../job.ts";
import { ServerStatuses } from "../server-status.ts";

interface IStatus {
  domain: string;
  port: number;
  status_code: number;
  response_ip: string;
  response_code: number;
  response_time: number;
}

export class WebsiteJob extends Job {
  private readonly api = 'https://isitup.org';

  public async execute() {
    // Make sure a website was specified
    if(!('host' in this.opts)) {
      await ServerStatuses.update('Website', 'unknown');
      this.addError('No host was specified!');
      return;
    }

    // Run test
    let resp: IStatus;
    try {
      resp = await this.fetch(`${this.api}/${this.opts.host!}.json`, {
        method: 'GET'
      }).then(resp => resp.json());
    } catch(e) {
      this.addError(`Could not test website status: ${e.message}`);
      return false;
    }

    // If status was 200, report we're up
    if(resp.response_code === 200) {
      await ServerStatuses.update('Website', 'up');
      return;
    }

    // We didn't get a 200 response, so report we're down
    await ServerStatuses.update('Website', 'down');
  }
}
