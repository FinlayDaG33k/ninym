import { Job } from "../job.ts";
import { Database } from "../database.ts";
import { Logger } from "../../deps.ts";

enum Status {
  UNKNOWN = -1,
  FAIL = 0,
  OK= 1,
  SKIPPED = 2,
}

export class InternetJob extends Job {
  private readonly hosts: string[] = ['social.linux.pizza', 'www.gitlab.com', 'www.google.com'];
  private readonly steps: string[] = ['interface', 'address', 'ping', 'dns'];
  private status: {[key: string]: Status} = {};

  constructor(id: string, opts: any) {
    super(id, opts);
  }

  public async execute() {
    // Test overall connection
    // Run troubleshooter if need be
    if(await this.test() === 0) {
      // Run main troubleshooter
      await this.troubleshooter();
    } else {
      for(let i = 0; i < this.steps.length; i++) {
        this.status[this.steps[i]] = 1;
      }
    }

    // Update database
    try {
      await Database.set('internet-status', this.status);
    } catch(e) {
      this.addError(`Could not set database key: ${e.message}`);
    }
  }

  private async test(): Promise<number> {
    // Loop over each host
    // Try to get a response
    // Resolve when a response has been obtained
    // Keep going if one host isn't responding
    for (const host of this.hosts) {
      Logger.debug(`Testing reachability for "${host}"...`);
      let resp;
      try {
        resp = await this.fetch(`https://${host}`, {
          method: 'HEAD'
        });
      } catch(e) {
        continue;
      }

      if(resp.statusText === 'OK') {
        Logger.debug(`"${host}" was reachable!`);
        return Status.OK;
      }
    }

    // We haven't returned yet
    // Internet is likely down
    Logger.debug("No host was reachable!");
    return Status.FAIL;
  }

  private async troubleshooter(): Promise<void> {
    // Run all needed tests
    for(let i = 0; i < this.steps.length; i++) {
      // Check if previous test failed.
      // Skip current test if so as there's no point in testing.
      const previous = this.status[this.steps[i - 1]];
      if(previous === Status.FAIL || previous === Status.SKIPPED) {
        Logger.debug(`Skipping troubleshooter step "${this.steps[i]}"...`);
        this.status[this.steps[i]] = Status.SKIPPED;
        continue;
      }
      
      // Run the next step
      Logger.debug(`Running troubleshooter step "${this.steps[i]}"...`);
      this.status[this.steps[i]] = await this[this.steps[i]]();
    }
  }

  /**
   * Check whether our WAN interface is running.
   * This will return a failing status if not.
   * 
   * @returns Promise<number>
   */
  private async interface(): Promise<number> {
    let resp;
    try {
      const auth = btoa(`${this.opts.mt_user}:${this.opts.mt_pass}`);
      resp = await this.fetch(`${this.opts.mt_scheme}://${this.opts.mt_host}/rest/interface/${this.opts.mt_wan_interface}`, {
        method: 'GET',
        headers: {
          Authorization: `Basic ${auth}`,
          'Content-Type': 'application/json',
        },
      });
      resp = await resp.json();
    } catch(e) {
      this.addError(`Could not test interface status: "${e.message}"`);
      return -1;
    }

    // Return whether interface is running
    // MT returns status as a string, not a bool
    if(resp.running !== "true") {
      Logger.debug(`Interface "${this.opts.mt_wan_interface}" does not appear to be running!`);
      return Status.FAIL;
    }
    return Status.OK;
  }

  
  /**
   * Check whether we have a WAN address.
   * This will return a failing status if we do not.
   * 
   * @returns Promise<number>
   */
  private async address(): Promise<number> {
    let resp;
    try {
      const auth = btoa(`${this.opts.mt_user}:${this.opts.mt_pass}`);
      resp = await this.fetch(`${this.opts.mt_scheme}://${this.opts.mt_host}/rest/ip/address/print`, {
        method: 'POST',
        headers: {
          Authorization: `Basic ${auth}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          ".proplist": ["interface", "address"],
          ".query": [`interface=${this.opts.mt_wan_interface}`],
        }),
      });
      resp = await resp.json();
    } catch(e) {
      this.addError(`Could not test interface address: "${e.message}"`);
      return -1;
    }

    // Return whether interface has an IP
    if(resp.length === 0) {
      Logger.debug(`We have no address on interface "${this.opts.mt_wan_interface}"!`);
      return Status.FAIL;
    }
    return Status.OK;
  }

  /**
   * Check whether we can ping Google's DNS (8.8.8.8) via the router.
   * This will return a failing status if not a single packet came back.
   * 
   * @returns Promise<number>
   */
  private async ping(): Promise<number> {
    let resp;
    try {
      const auth = btoa(`${this.opts.mt_user}:${this.opts.mt_pass}`);
      resp = await this.fetch(`${this.opts.mt_scheme}://${this.opts.mt_host}/rest/ping`, {
        method: 'POST',
        headers: {
          Authorization: `Basic ${auth}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "address": "8.8.8.8",
          "count": 4,
        }),
      });
      resp = await resp.json();
    } catch(e) {
      this.addError(`Could not test ping status: "${e.message}"`);
      return -1;
    }

    // Return whether we were able to ping Google's DNS
    Logger.debug(`Got a packet loss of ${resp[3]['packet-loss']}%!`);
    if(resp[3]['packet-loss'] < 100) return Status.OK;
    return Status.FAIL;
  }

  /**
   * Check whether we could resolve A records for out test hosts.
   * This will return a failing status if no host could be resolved.
   * 
   * @returns Promise<number>
   */
  private async dns(): Promise<number> {
    let failures = 0;
    
    for(const host of this.hosts) {
      Logger.debug(`Testing DNS status for "${host}"...`);
      try {
        const controller = new AbortController();
        setTimeout(() => controller.abort(), 1000);
        await Deno.resolveDns(host, "A", {signal: controller.signal});
        Logger.debug(`DNS for "${host}" succeeded!`);
        return Status.OK;
      } catch(e) {
        this.addError(`Could not test DNS status for "${host}": "${e.message}"`);
        failures++;
      }
    }
    
    // Check if we have 2 or more failures
    Logger.debug("No DNS could be resolved!");
    return Status.FAIL;
  }
}
