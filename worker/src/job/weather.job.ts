import { Job } from "../job.ts";
import { Database } from "../database.ts";

export class WeatherJob extends Job {
  private readonly owm_host = 'https://api.openweathermap.org/data/3.0/onecall';

  public async execute() {
    // Make sure an OWM key was specified
    if(!('owm_key' in this.opts)) {
      this.addError('No API key was specified!');
      return;
    }

    // Make sure an OWM lat was specified
    if(!('owm_lat' in this.opts)) {
      this.addError('No latitude was specified!');
      return;
    }

    // Make sure an OWM lon was specified
    if(!('owm_lon' in this.opts)) {
      this.addError('No longitude was specified!');
      return;
    }

    // Download data from API
    let resp;
    try {
      resp = await this.fetch(`${this.owm_host}?units=metric&lat=${this.opts.owm_lat!}&lon=${this.opts.owm_lon!}&appid=${this.opts.owm_key!}`)
        .then(response => response.json());
    } catch(e) {
      this.addError(`Could not get weather from OWM: "${e.message}"`);
      return;
    }

    // Store the weather data in database
    try {
      await Database.set('weather', resp);
    } catch(e) {
      this.addError(`Could not set database key: ${e.message}`);
    }
  }
}
