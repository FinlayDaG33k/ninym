import { DOMParser } from "../../deps.ts";
import { Job } from "../job.ts";
import { Database } from "../database.ts";

interface Vendor {
  vendor: string;
  type: string;
  price: number;
}

export class FuelJob extends Job {
  private data: Vendor[] = [];

  public async execute() {
    // Get the data from our sources
    await Promise.all([
      this.updateMakro(),
      this.updateStar(),
    ]);
    
    // Update database
    try {
      await Database.set('fuel-prices', this.data);
    } catch(e) {
      this.addError(`Could not set database key: ${e.message}`);
    }
  }

  private async updateMakro(): Promise<void> {
    // Send request
    const controller = new AbortController();
    setTimeout(() => controller.abort(), 5000);
    let page;
    try {
      page = await fetch('https://www.makro.nl/vestigingen/duiven', {
        method: 'GET',
        signal: controller.signal,
      }).then(resp => resp.text());
    } catch(e) {
      if(e.name === 'AbortError') {
        this.addError(`Could not update Makro price: Aborted after 5 seconds.`);
        return;
      }
      this.addError(`Could not update Makro price: "${e.message}"`);
      return;
    }

    // Parse the page HTML
    let doc;
    try {
      const parser = new DOMParser();
      doc = parser.parseFromString(page, 'text/html');
    } catch(e) {
      this.addError(`Could not parse HTML for Makro price: "${e.message}`);
      return;
    }
    if(!doc) {
      this.addError(`Could not update Makro price: "Empty body!"`);
      return;
    }

    // Get the data from the pace
    const entries = doc.querySelectorAll(`div.prices.slides div.price.slide.element-position`);
    for(const entry of entries) {
      const type = entry.querySelector('div.field-name');
      switch(type.textContent) {
        case "Superplus": {
          const price = entry.querySelector('div.field-price');
          this.data.push({
            vendor: 'Makro', 
            type: 'E5', 
            price: parseFloat(price.textContent.replace('€', '').replace(',', '.'))
          });
          break;
        }
        case "Euro 95 (E10)": {
          const price = entry.querySelector('div.field-price');
          this.data.push({
            vendor: 'Makro',
            type: 'E10',
            price: parseFloat(price.textContent.replace('€', '').replace(',', '.'))
          });
          break;
        }
      }
    }
  }
  
  private async updateStar(): Promise<void> {
    // Send request
    let page;
    try {
      page = await this.fetch('https://ich-tanke.de/tankstelle/05fc80bf318dd15bb1a142836b7e2731/', {
        method: 'GET',
      }).then(resp => resp.text());
    } catch(e) {
      if(e.name === 'AbortError') {
        this.addError(`Could not update Star price: Aborted after 5 seconds.`);
        return;
      }
      this.addError(`Could not update Star price: "${e.message}"`);
      return;
    }

    // Parse the page HTML
    let doc;
    try {
      const parser = new DOMParser();
      doc = parser.parseFromString(page, 'text/html');
    } catch(e) {
      this.addError(`Could not parse HTML for Star price: "${e.message}`);
      return;
    }
    if(!doc) {
      this.addError(`Could not update Star price: "Empty body!"`);
      return;
    }
    
    // Get the data from the page
    const entries = doc.querySelectorAll(`div#tankstelle-preis div.preis`);
    for(const entry of entries) {
      const type = entry.querySelector('strong');
      switch(type.textContent) {
        case "Super Benzin": {
          const price = entry.querySelector('span.zahl');
          if(price.textContent.replace('€', '') === '×,×××') {
            this.data.push({
              vendor: 'Star',
              type: 'E5',
              price: 0.00
            });
            break;
          }
          this.data.push({
            vendor: 'Star',
            type: 'E5',
            price: parseFloat(price.textContent.replace('€', '').replace(',', '.'))
          });
          break;
        }
        case "Super (E10) Benzin": {
          const price = entry.querySelector('span.zahl');
          if(price === '×,×××') {
            this.data.push({
              vendor: 'Star',
              type: 'E10',
              price: 0.00
            });
            break;
          }
          this.data.push({
            vendor: 'Star',
            type: 'E10',
            price: parseFloat(price.textContent.replace('€', '').replace(',', '.'))
          });
          break;
        }
      }
    }
  }
}
