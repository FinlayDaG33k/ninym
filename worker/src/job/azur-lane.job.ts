import { Job } from "../job.ts";
import { ServerStatuses } from "../server-status.ts";

interface IServer {
  id: number;
  name: string;
  state: number;
  flag: number;
  sort: number;
}

export class AzurLaneJob extends Job {
  private api = 'http://blhxusgate.yo-star.com/?cmd=load_server?';

  constructor(id: string, opts: any) {
    super(id, opts);
  }

  public async execute() {
    // Make sure a server was specified
    if(!('server' in this.opts)) {
      await ServerStatuses.update('Azur Lane', 'unknown');
      this.addError('No server was specified!');
      return;
    }

    // Get data from server
    let resp;
    try {
      resp = await this.fetch(this.api, { method: 'GET' });
      resp = await resp.json();
    } catch(e) {
      await ServerStatuses.update('Azur Lane', 'unknown');
      if(e.name === 'AbortError') {
        this.addError(`Could not obtain status for Azur Lane: Aborted after 5 seconds.`);
        return;
      }
      this.addError(`Received error from server: "${e.message}"`);
      return;
    }

    // Find and make sure the server exists
    const server: IServer = resp.find((server: IServer) => server.name === this.opts.server!);
    if(!server) {
      await ServerStatuses.update('Azur Lane', 'unknown');
      this.addError(`Server "${this.opts.server!}" could not be found!`);
      return;
    }

    // Report the corresponding result
    switch(server.state) {
      case 0:
        await ServerStatuses.update('Azur Lane', 'up');
        break;
      case 1:
        await ServerStatuses.update('Azur Lane', 'down');
        break;
      default:
        await ServerStatuses.update('Azur Lane', 'unknown');
        break;
    }
  }
}
