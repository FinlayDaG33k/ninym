import { Logger, Ntfy, Nut } from "../../deps.ts";
import { Job } from "../job.ts";
import { Database } from "../database.ts";

interface IUpsState {
  maxWatt: number;
  loadPercent: number;
  loadWatt: number;
  chargePercent: number;
  remainingSeconds: number;
  status: string;
}

export class UpsJob extends Job {
  private nut: Nut|null = null;
  private prevStatus = 'OL';
  private state: IUpsState = {
    maxWatt: 0,
    loadPercent: 0,
    loadWatt: 0,
    chargePercent: 0,
    remainingSeconds: 0,
    status: ''
  }

  public async execute() {
    // Make sure a NUT host was specified
    if(!('server' in this.opts)) {
      this.addError('No server was specified!');
      return;
    }

    // Make sure an UPS was specified
    if(!('ups' in this.opts)) {
      this.addError('No UPS was specified!');
      return;
    }

    // Get previous status from database
    try {
      let data = await Database.get(`ups-state`);
      if(!data) data = {
        maxWatt: 0,
        loadPercent: 0,
        loadWatt: 0,
        chargePercent: 0,
        remainingSeconds: 0,
        status: ''
      };
      if('status' in data) this.prevStatus = data.status;
    } catch(e) {
      this.addError(`Could not parse previous status from database: "${e.message}"`);
      return;
    }

    // Connect to NUT
    try {
      this.nut = new Nut(this.opts.server!);
      await this.nut.connect();
    } catch (e) {
      this.addError(`Could not connect to NUT server: "${e.message}"`);
      return;
    }

    // Obtain max UPS load
    try {
      Logger.debug(`Obtaining max load for UPS "${this.opts.ups}"...`);
      this.state.maxWatt = await this.nut.getPowerLimit(this.opts.ups);
      Logger.debug(`Max load for UPS "${this.opts.ups}" reported as "${this.state.maxWatt}W"`);
    } catch (e) {
      this.addError(`Could not obtain max UPS load: "${e.message}"`);
      return;
    }

    // Get the current load
    try {
      Logger.debug(`Obtaining current load for UPS "${this.opts.ups}"...`);
      this.state.loadPercent = await this.nut.getLoad(this.opts.ups);
      Logger.debug(`Current load for UPS "${this.opts.ups}" reported as "${this.state.loadPercent}%"`);
    } catch (e) {
      this.addError(`Could not obtain current UPS load (percentage): "${e.message}"`);
      return;
    }

    // Calculate the load in Watts
    try {
      this.state.loadWatt = (this.state.loadPercent > 0) ? Number((( this.state.loadPercent *  this.state.maxWatt) / 100).toFixed(2)) : 0;
      Logger.debug(`Current load for UPS "${this.opts.ups}" reported as "${this.state.loadWatt}W"`);
    } catch (e) {
      this.addError(`Could not obtain current UPS load (watts): "${e.message}"`);
      return;
    }

    // Get the current battery charge
    try {
      Logger.debug(`Obtaining current charge for UPS "${this.opts.ups}"...`);
      this.state.chargePercent = await this.nut.getCharge(this.opts.ups);
      Logger.debug(`Current charge for UPS "${this.opts.ups}" reported as "${this.state.chargePercent}%"`);
    } catch (e) {
      this.addError(`Could not obtain current battery charge: "${e.message}"`);
      return;
    }

    // Get the estimated remaining runtime
    try {
      Logger.debug(`Obtaining estimated runtime left for UPS "${this.opts.ups}"...`);
      this.state.remainingSeconds = await this.nut.getRuntime(this.opts.ups);
      Logger.debug(`Estimated runtime left for UPS "${this.opts.ups}" reported as "${this.state.remainingSeconds}s"`);
    } catch (e) {
      this.addError(`Could not obtain estimated run time: "${e.message}"`);
      return;
    }

    // Get status
    try {
      Logger.debug(`Obtaining status of UPS "${this.opts.ups}"...`);
      this.state.status = await this.nut.getStatus(this.opts.ups);
      Logger.debug(`Status for UPS "${this.opts.ups}" reported as "${this.state.status}"`);
    } catch (e) {
      this.addError(`Could not obtain UPS status: "${e.message}"`);
      return;
    }

    // Send notification if status changed
    // Then update the previous status to prevent dupes
    if(this.prevStatus !== this.state.status) await this.notify();

    // Update database
    await Database.set(`ups-state`, this.state);
  }

  private async notify() {
    if(!('ntfy_host' in this.opts)) {
      this.addError('No "ntfy_host" set!');
      return;
    }
    if(!('ntfy_username' in this.opts)) {
      this.addError('No "ntfy_username" set!');
      return;
    }
    if(!('ntfy_password' in this.opts)) {
      this.addError('No "ntfy_password" set!');
      return;
    }

    const ntfy = new Ntfy(
      this.opts.ntfy_host,
      'Ninym',
      this.opts.ntfy_username,
      this.opts.ntfy_password
    );
    switch(this.state.status) {
      case 'OL':
        await ntfy.send(`UPS Status changed to "AC"`);
        break;
      case 'LO':
        await ntfy.send(`UPS Status changed to "Battery"`);
        break;
    }
  }
}
