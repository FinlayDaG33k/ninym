import { Job } from "../job.ts";
import { Database } from "../database.ts";

interface ITradePair {
  crypto: string;
  fiat: string;
  value: number;
  change: number;
}

interface ITicker {
  bid: number;
  bid_size: number;
  ask: number;
  ask_size: number;
  daily_change: number;
  daily_change_relative: number;
  last_price: number,
  volume: number;
  high: number,
  low: number
}

export class CryptoJob extends Job {
  private api: string = 'https://api-pub.bitfinex.com/v2/ticker';
  private tickers: ITradePair[] = [];

  public async execute() {
    // Add our initial tradepairs
    this.tickers.push({crypto: 'BTC', fiat: 'USD', value: 0.00, change: 0.00});
    this.tickers.push({crypto: 'BTC', fiat: 'EUR', value: 0.00, change: 0.00});
    this.tickers.push({crypto: 'ETH', fiat: 'USD', value: 0.00, change: 0.00});
    this.tickers.push({crypto: 'ETH', fiat: 'EUR', value: 0.00, change: 0.00});

    // Get current prices for each ticket
    for (const pair of this.tickers) {
      const data = await this.updatePair(`${pair.crypto.toUpperCase()}${pair.fiat.toUpperCase()}`);
      pair.value = data[0] ?? 0.00;
      pair.change = data[5] ?? 0.00;
    }

    // Update database
    try {
      await Database.set('crypto-tickers', this.tickers);
    } catch(e) {
      this.addError(`Could not set database key: ${e.message}`);
    }
  }

  private async updatePair(pair: string): Promise<number[]> {
    let resp;
    try {
      resp = await this.fetch(`${this.api}/t${pair}`, { method: 'GET' });
      if(resp.status !== 200) return [];
    } catch(e) {
      if(e.name === 'AbortError') {
        this.addError(`Could not update ticker "${pair}": Aborted after 5 seconds.`);
        return [];
      }
      this.addError(`Could not update ticker "${pair}": ${e.message}`);
      return [];
    }

    return await resp.json();
  }
}
