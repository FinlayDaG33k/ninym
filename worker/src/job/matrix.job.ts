import { Job } from "../job.ts";
import { ServerStatuses } from "../server-status.ts";

export class MatrixJob extends Job {
  public async execute() {
    // Make sure a server was specified
    if(!('host' in this.opts)) {
      await ServerStatuses.update('Matrix', 'unknown');
      this.addError('No host was specified!');
      return;
    }

    await ServerStatuses.update('Matrix', await this.getStatus());
  }

  public async getStatus(): Promise<string> {
    // Try to get a response from the federation tester
    let resp;
    try {
      resp = await this.fetch(`https://federationtester.matrix.org/api/report?server_name=${this.opts.host!}`)
        .then(response => response.json());
    } catch (e) {
      this.addError(`Could not get federation test: ${e.message}`);
      return 'unknown';
    }

    return resp.FederationOK ? 'up' : 'down';
  }
}
