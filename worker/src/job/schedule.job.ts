import { Time } from "../../deps.ts";
import { Job } from "../job.ts";
import { Database } from "../database.ts";

export class ScheduleJob extends Job {
  private icsContent: any = {};
  private appointments: any[] = [];

  public async execute() {
    // Make sure an url was specified
    if(!('url' in this.opts)) {
      this.addError('No url specified!');
      return;
    }

    // Make sure an username was specified
    if(!('username' in this.opts)) {
      this.addError('No username specified!');
      return;
    }

    // Make sure a password was specified
    if(!('password' in this.opts)) {
      this.addError('No password specified!');
      return;
    }

    // Download our ICS file
    const ics = await this.download();

    // Parse our ICS file
    this.icsContent = await this.parse(ics);

    // Get relevant appointments
    await this.week();

    // Push to database
    try {
      await Database.set('ical-schedule', this.appointments);
    } catch(e) {
      this.addError(`Could not set database key: ${e.message}`);
    }
  }

  private async download() {
    const creds = btoa(`${this.opts.username!}:${this.opts.password!}`);

    return await this.fetch(this.opts.url!, {
      headers: {
        'Authorization': `Basic ${creds}`
      }
    }, 10_000).then(response => response.text());
  }

  private async parse(ics: string) {
    // Split at each new line
    const lines = ics.split("\r\n");

    // Hold our events
    const events: any = {};
    let events_i = 0;

    // Loop over each line in our ICS
    // Add them to our events
    let hasEvent = false;
    for (let i = 0; i < lines.length; i++) {
      // Go until we find an event
      // Create a new event object if we do
      if(lines[i] !== 'BEGIN:VEVENT' && hasEvent == false) continue;
      if(lines[i] === 'BEGIN:VEVENT') {
        hasEvent = true;
        events[events_i] = {
          date: null,
          title: null
        };
      }

      if (lines[i].includes('DTSTART')) {
        const date = lines[i].split(":");
        events[events_i].date = this.getDate(date[1]);
        continue;
      }

      if (lines[i].includes('SUMMARY')) {
        const title = lines[i].split(":");
        events[events_i].title = title[1];
        continue;
      }

      if (lines[i].includes('END:VEVENT')) {
        hasEvent = false;
        events_i++;
        continue;
      }
    }

    return events;
  }

  private async week() {
    // Get all events for the coming two weeks
    const weekEventsKeys = Object.keys(this.icsContent).filter(key => {
      const entry = this.icsContent[key].date;
      const today = new Time().midnight();
      const week = new Time().addWeek(2).midnight();

      // Filter everything
      // - Before today
      // - After 2 weeks ahead
      if(entry.getTime < today.getTime) return false;
      if(entry.getTime > week.getTime) return false;

      // Return leftovers
      return true;
    }).reverse();

    // Make sure we have entries
    if(weekEventsKeys.length === 0) {
      this.appointments = [];
      return;
    }

    // Add all appointments
    this.appointments = [];
    for(const eventKey of weekEventsKeys) {
      const event = this.icsContent[eventKey];
      this.appointments.push(event);
    }

    // Sort appointments
    this.appointments.sort((a: any, b: any) => {
      if(a.date.getTime < b.date.getTime) return -1;
      if(a.date.getTime > b.date.getTime) return 1;
      return 0;
    });
  }

  private getDate(date: string): Time {
    // Build our regex
    const regex = /([0-9]{4})([0-9]{2})([0-9]{2})T?([0-9]{2})?([0-9]{2})?([0-9]{2})?([A-Z])?/;

    // Execute our regex
    const matches = regex.exec(date);

    // Exit if no matches found
    if(!matches) throw new Error('Could not parse date!');

    // Building the date in a proper format
    let datestring = '';
    if(matches[1]) datestring += matches[1];
    if(matches[2]) datestring += '-';
    if(matches[2]) datestring += matches[2];
    if(matches[3]) datestring += '-';
    if(matches[3]) datestring += matches[3];
    if(matches[4]) datestring += 'T';
    if(matches[4]) datestring += matches[4];
    if(matches[5]) datestring += ':';
    if(matches[5]) datestring += matches[5];
    if(matches[6]) datestring += ':';
    if(matches[6]) datestring += matches[6];
    if(matches[7]) datestring += matches[7];

    // Return a new Time instance
    return new Time(datestring);
  }
}
