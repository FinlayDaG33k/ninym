import { Logger, DOMParser, Element, sleep } from "../../deps.ts";
import { Job } from "../job.ts";
import { Database } from "../database.ts";

interface SeriesInfo {
  id: string;
  name: string;
  rating: number|string;
  progress: string;
  status: number|string;
}

enum SeriesStatus {
  UNKNOWN,
  DROPPED,
  BACKLOGGED,
  COMPLETED,
  WATCHING,
}

export class AnidbJob extends Job {
  private readonly host: string = `https://anidb.net`;
  private nextUrl: string = '';
  private list: SeriesInfo[] = [];

  public async execute() {
    if(!('user' in this.opts)) {
      this.addError('No user specified!');
      return;
    }

    // Set our first URL
    this.nextUrl = `/user/${this.opts.user!}/mylist/?orderby.name=1.1&orderby.vote=0.2&page=0`

    // Update the list
    try {
      let current = 1;
      while(this.nextUrl != '') {
        // Download the HTML for the current page
        Logger.debug(`Downloading HTML for page #${current}`);
        const html = await this.downloadPage();

        // Make sure a page was downloaded
        if(!html) break;

        // Parse the list and see if we have more
        Logger.debug(`Parsing HTML for page #${current}`);
        await this.getList(html);
        await sleep(1);
        current++;
      }
    } catch(e) {
      this.addError(`Could not update list: "${e.message}"`);
      return;
    }

    // Sort our list by conditions:
    // 1. Status (see AnidbJob#parseStatus)
    // 2. Rating
    // 3. Alphabetical
    this.list.sort((a: SeriesInfo, b: SeriesInfo) =>
      (b.status as number) - (a.status as number) ||
      (b.rating as number) - (a.rating as number) ||
      a.name.localeCompare(b.name)
    );

    // Replace statuses and ratings
    this.list = this.list.map((entry: SeriesInfo) => {
      switch(entry.status) {
        case 1:
          entry.status = 'Dropped';
          break;
        case 2:
          entry.status = 'Backlogged';
          break;
        case 3:
          entry.status = 'Completed';
          break;
        case 4:
          entry.status = 'Watching';
          break;
        case 0:
        default:
          entry.status = 'Unknown';
      }

      entry.rating = entry.rating === -1 ? 'TBD' : entry.rating;

      return entry;
    });

    // Update database
    try {
      await Database.set('anidb-list', this.list);
    } catch(e) {
      this.addError(`Could not set database key: ${e.message}`);
    }
  }

  private async downloadPage(): Promise<string|null> {
    // Send a request
    let resp;
    try {
      resp = await this.fetch(`${this.host}${this.nextUrl}`, {
        headers: {
          Accept: 'text/html',
          'User-Agent': 'Ninym (https://gitlab.com/finlaydag33k/ninym; contact@finlaydag33k.nl)'
        },
      }).then(resp => resp.text());
    } catch(e) {
      if(e.name === 'AbortError') {
        this.addError(`Could not obtain AniDB: Aborted after 5 seconds.`);
        return null;
      }
      this.addError(`Could not get page: "${e.message}"`);
      return null;
    }

    return resp;
  }

  private async getList(page: string): Promise<void> {
    // Parse the page HTML
    let doc;
    try {
      const parser = new DOMParser();
      doc = parser.parseFromString(page, 'text/html');
    } catch(e) {
      Logger.error(`Could not parse HTML: "${e.message}`);
      this.nextUrl = '';
      return;
    }

    // Make sure parsing went right
    if(!doc) {
      this.nextUrl = '';
      return;
    }

    // Get all the entries on the page
    const entries = doc.querySelectorAll(`table#animelist.animelist tbody > tr`);
    Logger.debug(`Found "${entries.length}" entries!`);

    // Parse all entries
    for await(const entry of entries){
      // Cast entry to Element
      const entryElement = <Element>entry;

      // Obtain all data
      const nameElem = entryElement.querySelector(`td.name > a`);
      const ratingElem = entryElement.querySelector(`td.vote`);
      const progressElem = entryElement.querySelector(`td.stats.seen`);
      const statusElem = entryElement.querySelector(`td.name > span.icons > a.i_icon`);

      // Make sure required data is present
      if(!nameElem) {
        Logger.debug('Could not find nameElem!');
        continue;
      }
      if(!ratingElem)  {
        Logger.debug('Could not find ratingElem!');
        continue;
      }
      if(!progressElem)  {
        Logger.debug('Could not find progressElem!');
        continue;
      }
      if(!statusElem) Logger.debug('Could not find statusElem!');

      // Parse the status
      let status: SeriesStatus;
      if(statusElem !== null) {
        status = AnidbJob.parseStatus(statusElem.classList);
      } else {
        status = SeriesStatus.UNKNOWN;
      }

      // Set the rating to -1 ("TBD") if not set in AniDB or still in watching
      let rating = -1;
      if(ratingElem.textContent !== '' && ![2,4].includes(status)) {
        rating = parseFloat(ratingElem.textContent);
      }

      // Add entry to list
      this.list.push({
        id: nameElem.getAttribute("href")?.replace('/anime/', '') ?? '',
        name: nameElem.textContent,
        rating: rating,
        progress: progressElem.textContent.replace('\n\t\t', ''),
        status: status
      });
    }

    // Check if there is a next page
    const nextButton = doc.querySelector(`div.g_content.mylist_all ul.g_list.jump li.next a`);
    if(!nextButton) {
      this.nextUrl = '';
      return;
    }
    this.nextUrl = nextButton.getAttribute("href") ?? '';
  }

  /**
   * Parse status to a number for easier grouping and sorting later
   * Watching + Collecting are intentionally put higher than completed.
   *
   * @param classList
   * @returns SeriesStatus
   */
  private static parseStatus(classList: any): SeriesStatus {
    if(classList.contains('i_liststate_completed')) return SeriesStatus.COMPLETED;
    if(classList.contains('i_liststate_collecting')) return SeriesStatus.BACKLOGGED;
    if(classList.contains('i_liststate_watching')) return SeriesStatus.WATCHING;
    if(classList.contains('i_liststate_dropped')) return SeriesStatus.DROPPED;
    if(classList.contains('i_liststate_stalled')) return SeriesStatus.BACKLOGGED;
    return SeriesStatus.UNKNOWN;
  }
}
