import { Job } from "../job.ts";
import { Database } from "../database.ts";
import { parseFeed } from "../../deps.ts";

interface NewsItem {
  source?: string;
  url: string;
  title: string;
  added: Date;
  modified?: Date;
}

interface NewsSource {
  name: string;
  url: string;
}

export class RssJob extends Job {
  private readonly sources: NewsSource[] = [
    { name: 'Wikichip Fuse' , url: 'https://fuse.wikichip.org/feed' },
    { name: 'Phoronix', url: 'https://www.phoronix.com/rss.php' },
    { name: 'The Hacker News', url: 'https://feeds.feedburner.com/TheHackersNews?fmt=xml' },
    { name: 'The Verge', url: 'https://www.theverge.com/rss/policy/index.xml' },
    { name: 'Gamers Nexus', url: 'https://gamersnexus.net/rss.xml' },
    { name: 'Videocardz.com', url: 'https://videocardz.com/rss-feed' },
    { name: 'MikroTik', url: 'https://mikrotik.com/download.rss' },
    { name: 'Forgejo', url: 'https://forgejo.org/releases/rss.xml' },
    { name: 'Proxmox', url: 'https://my.proxmox.com/en/announcements/tag/proxmox-ve/rss' },
    { name: 'Garage S3', url: 'https://git.deuxfleurs.fr/Deuxfleurs/garage/releases.rss' },
  ];

  public async execute() {
    // Create promise for each of the feeds
    const proms: any = [];
    for(const source of this.sources) {
      proms.push(this.getFeed(source));
    }

    // Await all promises
    const resp: any = await Promise.all(proms);

    // Put all data into a single array
    // Then sort them all based on publishing time
    let feed = [].concat(...resp.values());
    feed.sort((a: NewsItem, b: NewsItem) =>b.added.getTime() - a.added.getTime());
    feed = feed.slice(0, 100);

    // Update database
    try {
      await Database.set('rss-feed', feed);
    } catch(e) {
      this.addError(`Could not update database: "${e.message}"`);
    }
  }

  private async getFeed(source: NewsSource, limit = 10) {
    // Download and parse the feed from the RSS provider
    // Add an AbortController to limit runtime to 5 seconds.
    let resp;
    try {
      resp = await this.fetch(source.url);
    } catch(e) {
      if(e.name === 'AbortError') {
        this.addError(`Could not obtain RSS for "${source.name}": Aborted after 5 seconds.`);
        return;
      }
      this.addError(`Could not obtain RSS for "${source.name}": ${e.message}`);
      return;
    }
    
    // Check if we got an OK status
    // Parse the feed if we did
    if(resp.statusText !== 'OK') {
      this.addError(`Could not obtain RSS for "${source.name}": ${resp.statusText} (${resp.status})`);
      return;
    }
    const xml = await resp.text();
    const feed = await parseFeed(xml);

    // Loop over the feed
    // Add latest N entries to array
    const news: NewsItem[] = [];
    for(const entry of feed.entries) {
      // Make sure values are present
      if(typeof entry.title === 'undefined') {
        this.addError(`Error on article for "${source.name}": Item has no title`);
        continue;
      }
      if(typeof entry.title.value === 'undefined') {
        this.addError(`Error on article for "${source.name}": Item has no title`);
        continue;
      }
      if(typeof entry.published === 'undefined') {
        this.addError(`Error on article for "${source.name}": Item has no publication date`);
        continue;
      }
      if(typeof entry.updated === 'undefined') {
        entry.updated = entry.published;
      }

      // Add item to our array
      news.push({
        source: source.name,
        url: entry.links[0].href ?? "#",
        title: entry.title.value!,
        added: entry.published,
        modified: entry.updated
      });

      // Check if we have reached the limit
      // Break if we do
      if(news.length >= limit) break;
    }

    return news;
  }
}
