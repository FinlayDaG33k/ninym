import { Logger } from "../../deps.ts";
import { Job } from "../job.ts";
import { Database } from "../database.ts";

interface IDiskSpace {
  total: number;
  used: number;
  available: number;
  usePercent: number;
}

export class FilesystemJob extends Job {
  public async execute() {
    // Make sure a data_dir was specified
    if(!('data_dir' in this.opts)) {
      this.addError('No data_dir specified!');
      return;
    }

    // Get our diskspace
    const data = await this.getData();
    const regExp = /(?:[\/[a-zA-Z0-9:]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)%/gm;
    const matches = regExp.exec(data);
    if(matches === null) {
      this.addError('Could not obtain free diskspace!');
      Logger.debug(`Got data:\r\n${data}`);
      return;
    }
    const diskSpace: IDiskSpace = {
      total: Number(matches[1]) ?? 0,
      used: Number(matches[2]) ?? 0,
      available: Number(matches[3]) ?? 0,
      usePercent: Number(matches[4]) ?? 0
    };

    // Update database
    try {
      await Database.set('filesystem-usage', diskSpace);
    } catch(e) {
      this.addError(`Could not set database key: ${e.message}`);
    }
  }

  private async getData() {
    const cmd = Deno.run({
      cmd: [`df`, `-k`, `${this.opts.data_dir!}`],
      stdout: "piped",
      stderr: "piped"
    });
    const output = new TextDecoder().decode(await cmd.output());
    cmd.close();

    return output;
  }
}
