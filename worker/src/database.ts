import { Configure } from "../deps.ts";

export class Database {
  public static async get(key: string): Promise<string|null> {
    const resp = await fetch(`${Configure.get('master_host', 'http://localhost:8079')}/database/get/${key}`, {
      method: 'GET',
      headers: {
        authorization: `Bearer ${Configure.get('auth_secret')}`,
      },
    });

    // Get the content from the response
    const content = await resp.text();

    // Decode the JSON
    const data = JSON.parse(content);

    // Return our content
    return data.result;
  }

  public static async set(key: string, value: any) {
    const resp = await fetch(`${Configure.get('master_host', 'http://localhost:8079')}/database/set/${key}`, {
      method: 'PUT',
      headers: {
        authorization: `Bearer ${Configure.get('auth_secret')}`,
      },
      body: JSON.stringify(value)
    });

    // Get the content from the response
    const content = await resp.text();
    if(content !== 'OK') throw Error(content);
  }
}
