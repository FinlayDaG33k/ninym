FROM denoland/deno:alpine-1.30.0

ARG MODULE="master"
ARG TZ="Europe/Amsterdam"

# Set Timezone
RUN apk add tzdata && \
    cp /usr/share/zoneinfo/${TZ} /etc/localtime

WORKDIR /ninym/app
ADD ./${MODULE}/check.ts /ninym/app
ADD ./${MODULE}/entrypoint.ts /ninym/app
ADD ./${MODULE}/deps.ts /ninym/app
ADD ./${MODULE}/src /ninym/app/src
ADD ./${MODULE}/deno.prod.json /ninym/app/deno.json
RUN deno cache  deps.ts
RUN deno run --allow-all check.ts

CMD ["deno", "run", "--allow-all", "entrypoint.ts"]
