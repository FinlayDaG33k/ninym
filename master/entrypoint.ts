import { Cron, Logger, Webserver, Websocket, Configure } from "./deps.ts";
import { Queue } from "./src/queue.ts";

// Allow websocket in window object
declare global {
  interface Window {
    websocket: Websocket;
  }
}

// Load configure
await Configure.load();

// Load our routes and events
import "./src/routes.ts";
import "./src/events.ts";

// Start webserver
Logger.info(`Starting webserver on port "${Number(Configure.get("webserver_port", 80))}"...`);
const webserver = new Webserver(Number(Configure.get("webserver_port", 80)));
webserver.start();
Logger.info(`Webserver server started!`);

// Start websocket server
Logger.info(`Starting websocket server on port "${Number(Configure.get("websocket_port", 81))}"`);
window.websocket = new Websocket(Number(Configure.get("websocket_port", 81)), true);
await window.websocket.start();
Logger.info(`Websocket server started!`);

// Add task schedulers
Cron('* * * * *', async () => {
  await Queue.add('internet', {
    mt_scheme: Configure.get('mt_scheme', 'http'),
    mt_host: Configure.get('mt_host', '192.168.88.1'),
    mt_user: Configure.get('mt_user', 'admin'),
    mt_pass: Configure.get('mt_pass', ''),
    mt_wan_interface: Configure.get('mt_wan_interface', 'ether1'),
  });
  await Queue.add('azur-lane', { server: 'Washington' });
  await Queue.add('ups', {
    server: Configure.get("nut_host", null),
    ups: Configure.get("ups_name", 'main'),
    ntfy_host: Configure.get("ntfy_host", null),
    ntfy_username: Configure.get("ntfy_username", null),
    ntfy_password: Configure.get("ntfy_password", null),
  });
});

Cron('1 */10 * * * *', async () => {
  await Queue.add('crypto');
  await Queue.add('filesystem', {
    data_dir: Configure.get('filesystem_dir'),
  });
  await Queue.add('matrix', {
    host: Configure.get("matrix_host", null),
  });
  await Queue.add('weather', {
    owm_key: Configure.get("owm_key", null),
    owm_lat: Configure.get("owm_lat", null),
    owm_lon: Configure.get("owm_lon", null),
  });
  await Queue.add('schedule', {
    url: Configure.get("ics_url", null),
    username: Configure.get("ics_user", null),
    password: Configure.get("ics_pass", null),
  });
  //await Queue.add('website', { host: 'www.finlaydag33k.nl' });
});

Cron('1 0 * * * *', async () => {
  await Queue.add('rss');
  await Queue.add('fuel');
  await Queue.add('anilist', {
    shoko_host: Configure.get('shoko_host', 'http://localhost:8111'),
    shoko_user: Configure.get('shoko_user', 'default'),
    shoko_pass: Configure.get('shoko_pass', ''),
  });

  Queue.clean();
});
