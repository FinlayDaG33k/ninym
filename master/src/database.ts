import { Logger, Configure, CouchDB, Websocket } from "../deps.ts";

// Allow websocket in window object
declare global {
  interface Window {
    websocket: Websocket;
  }
}

interface IEvent {
  event: string;
  data: any;
}

export class Database {
  /**
   * Return an entry from the database
   * Will return null if key doesn't exist
   *
   * @param key
   * @returns any|null
   */
  public static async get(key: string): Promise<any | null> {
    const db = new CouchDB(
      Configure.get('couchdb_host', 'http://localhost:5984'),
      Configure.get('couchdb_database', 'ninym'),
      {
        username: Configure.get('couchdb_username', 'ninym'),
        password: Configure.get('couchdb_password', null),
      },
    );

    const document = await db.get(`item:${key}`);
    if(document.status === 404) return null;
    return document.data.data;
  }

  /**
   * Set an entry in the database.
   * This method will overwrite the old value if existing.
   *
   * @param key The key for the entry
   * @param value The value to set
   * @returns boolean Whether setting was a success
   */
  public static async set(key: string, value: any = null): Promise<boolean> {
    // Save our entries
    try {
      const db = new CouchDB(
        Configure.get('couchdb_host', 'http://localhost:5984'),
        Configure.get('couchdb_database', 'ninym'),
        {
          username: Configure.get('couchdb_username', 'ninym'),
          password: Configure.get('couchdb_password', null),
        },
      );

      const resp = await db.upsert(`item:${key}`, {data: value});
      if(resp.status !== 201) throw Error(`${resp.status} - ${resp.error.reason}`);
    } catch(e) {
      Logger.error(`Could not save key "${key}" to database: "${e.message}"`, e.stack);
    }

    // Try to dispatch an event through the websockets
    window.websocket.broadcast('DATABASE_UPDATE', {
      key: key,
      content: JSON.stringify(value),
    });

    return true;
  }

  /**
   * Delete an entry from the database.
   *
   * @param key The key to delete
   * @returns boolean Whether deletion was a success
   */
  public static async delete(key: string): Promise<boolean> {
    try {
      const db = new CouchDB(
        Configure.get('couchdb_host', 'http://localhost:5984'),
        Configure.get('couchdb_database', 'ninym'),
        {
          username: Configure.get('couchdb_username', 'ninym'),
          password: Configure.get('couchdb_password', null),
        },
      );

      const existing = await db.get(`item:${key}`);
      if(existing.status === 404) throw Error('Item does not exist');

      const deletion = await db.delete(`item:${key}`, existing.data['_rev']);
      // TODO: Handle deletion error

    } catch(e) {
      Logger.error(`Could not delete database entry "${key}": "${e.message}"`, e.stack);
      return false;
    }

    return true;
  }
}
