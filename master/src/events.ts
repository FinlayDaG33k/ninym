import { Events } from "../deps.ts";

await Events.add({name: 'ClientConnect', handler: 'client-connect'});
await Events.add({name: 'UpdateAvailability', handler: 'update-availability'});
