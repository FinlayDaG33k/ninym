import { Time, Logger, StatusCodes } from "../../deps.ts";
import { AppController } from "./app.controller.ts";
import { Database } from "../database.ts";


export interface ServerStatus {
  server: string;
  status: string;
  time: Time;
}

export class StatusController extends AppController {
  private statuses: ServerStatus[] = [];

  public async initialize() {
    await super.initialize();

    // Load components
    await this.loadComponent('Auth');
  }
  
  public async update() {
    // We always respond with plaintext
    this.getResponse().withType('text/plain');
    
    // Make sure we are authenticated
    if(!this.Auth.isAuthorized()) {
      this.set('message', 'ERR_INVALID_BEARER_TOKEN');
      this.getResponse().withStatus(StatusCodes.UNAUTHORIZED);
      return;
    }
    
    // Make sure a name is set
    if(!this.getRequest().getParam('name')) {
      this.set('message', 'ERR_EMPTY_NAME');
      this.getResponse().withStatus(StatusCodes.BAD_REQUEST);
      return;
    }

    // Make sure a body is set
    if(!this.getRequest().getBody()) {
      this.set('message', 'ERR_EMPTY_STATUS_BODY');
      this.getResponse().withStatus(StatusCodes.BAD_REQUEST);
      return;
    }

    // Make sure the body has one of the allowed statuses
    const allowed = ['up', 'down', 'unknown'];
    if(allowed.indexOf(this.getRequest().getBody()) === -1) {
      this.set('message', 'ERR_INVALID_STATUS');
      this.getResponse().withStatus(StatusCodes.BAD_REQUEST);
      return;
    }

    // Get our status object from database
    try {
      const statuses = await Database.get(`server-statuses`);
      this.statuses = statuses === null ? [] : statuses;
    } catch(e) {
      Logger.error(`Could not get key "server-statuses" from database: "${e.message}"`, e.stack);
      this.set('message', 'INTERNAL_SERVER_ERROR');
      this.getResponse().withStatus(StatusCodes.INTERNAL_SERVER_ERROR);
      return;
    }

    // Replace %20 with actual spaces
    const name = this.getRequest().getParam('name').replace('%20', ' ');

    // Check if this service already exists
    // If so, update it
    // Else, add it
    const index = this.statuses.findIndex((item: ServerStatus) => item.server === name);
    if(index >= 0) {
      this.statuses[index].time = new Time();
      this.statuses[index].status = this.getRequest().getBody();
    } else {
      this.statuses.push({
        server: name,
        status: this.getRequest().getBody(),
        time: new Time()
      });
    }

    // Send our updated status to the database
    try {
      await Database.set(`server-statuses`, this.statuses);
    } catch(e) {
      Logger.error(`Could not set key "server-statuses" in database: "${e.message}"`, e.stack);
      this.set('message', 'INTERNAL_SERVER_ERROR');
      this.getResponse().withStatus(StatusCodes.INTERNAL_SERVER_ERROR);
      return;
    }

    this.set('message', 'OK');
  }
}
