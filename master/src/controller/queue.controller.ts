import { StatusCodes } from "../../deps.ts";
import { AppController } from "./app.controller.ts";
import { Queue } from "../queue.ts";


export class QueueController extends AppController {

  public async initialize() {
    await super.initialize();

    // Load components
    await this.loadComponent('Auth');
  }
  
  public async check() {
    this.getResponse().withType('text/plain');
    this.set('message', Queue.hasJobs());
  }

  public async get() {
    // Make sure we are authenticated
    if(!this.Auth.isAuthorized()) {
      this.getResponse().withType('text/plain');
      this.set('message', 'ERR_INVALID_BEARER_TOKEN');
      this.getResponse().withStatus(StatusCodes.UNAUTHORIZED);
      return;
    }

    // Get a job
    const item = Queue.startItem();

    // Send job to worker
    this.getResponse().withType('application/json');
    this.set('data', item);
  }

  public async report() {
    // Make sure we are authenticated
    if(!this.Auth.isAuthorized()) {
      this.getResponse().withType('text/plain');
      this.set('message', 'ERR_INVALID_BEARER_TOKEN');
      this.getResponse().withStatus(StatusCodes.UNAUTHORIZED);
      return;
    }

    // Get our data
    const data = JSON.parse(this.request.body);

    // Finish reporting to queue
    await Queue.reportItem(data.id, data.error);
    
    this.getResponse().withType('text/plain');
    this.set('message', 'OK');
  }

  public async dump() {
    // Make sure we are authenticated
    if(!this.Auth.isAuthorized()) {
      this.getResponse().withType('text/plain');
      this.set('message', 'ERR_INVALID_BEARER_TOKEN');
      this.getResponse().withStatus(StatusCodes.UNAUTHORIZED);
      return;
    }

    // get all items in the queue
    const items = Queue.dump();

    // Send all queue items
    this.getResponse().withType('application/json');
    this.set('data', items);
  }
}
