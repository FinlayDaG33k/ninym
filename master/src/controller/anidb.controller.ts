import { Cache } from "../../deps.ts";
import { AppController } from "./app.controller.ts";
import { Database } from "../database.ts";

const css = await Deno.readTextFile(`${Deno.cwd()}/src/templates/anidb/water.min.css`);

export class AnidbController extends AppController {
  /**
   * Returns a "nice" to view page
   */
  public async index() {
    // Get the data from Cache
    // If it doesn't exist, fetch it from Database
    // Cache data for 30 minutes
    let data = Cache.get('anidb-list');
    if(!data) {
      data = await Database.get('anidb-list');
      Cache.set('anidb-list', data, '+30 minutes');
    }

    // Set our view variables
    this.set('data', data ?? '[]');
    this.set('css', css);
  }

  /**
   * Returns the raw JSON from the database to the client
   */
  public async display() {
    // Get the data from Cache
    // If it doesn't exist, fetch it from Database
    // Cache data for 30 minutes
    let data = Cache.get('anidb-list');
    if(!data) {
      data = await Database.get('anidb-list');
      Cache.set('anidb-list', data, '+30 minutes');
    }

    // Return the data as JSON
    this.getResponse().withType('application/json');
    this.set('data', data ?? '[]');
  }
}
