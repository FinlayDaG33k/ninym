import { AppController } from "./app.controller.ts";

export class HomeController extends AppController {
  
  public async index() {
    this.getResponse().withType('text/plain');
    this.set('message', 'Ninym is up and running! :D');
  }
}
