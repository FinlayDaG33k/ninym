import { Configure, Component } from "../../../deps.ts";

export class AuthComponent extends Component {
  public isAuthorized(): boolean {
    return this.getController().getRequest().getAuth() === `Bearer ${Configure.get('auth_secret')}`;
  }
}
