import { StatusCodes } from "../../deps.ts";
import { AppController } from "./app.controller.ts";
import { Database } from "../database.ts";

export class DatabaseController extends AppController {
  public async initialize() {
    await super.initialize();

    // Load components
    await this.loadComponent('auth');
  }
  
  public async display() {
    // We always respond with JSON
    this.getResponse().withType('application/json');

    // Make sure we are authenticated
    if(!this.Auth.isAuthorized()) {
      this.set('data', { message: 'ERR_INVALID_BEARER_TOKEN' });
      this.getResponse().withStatus(StatusCodes.UNAUTHORIZED);
      return;
    }

    // Make sure a key is set
    if(!this.getRequest().getParam('key')) {
      this.set('data', { message: 'ERR_EMPTY_KEY' });
      this.getResponse().withStatus(StatusCodes.BAD_REQUEST);
      return;
    }

    // Get the item from our database and set our return data
    this.set('data', {
      result: await Database.get(this.getRequest().getParam('key')!),
    });
  }

  public async add() {
    // We always respond with plaintext
    this.getResponse().withType('text/plain');

    // Make sure we are authenticated
    if(!this.Auth.isAuthorized()) {
      this.set('message', 'ERR_INVALID_BEARER_TOKEN');
      this.getResponse().withStatus(StatusCodes.UNAUTHORIZED);
      return;
    }

    // Make sure a key is set
    if(!this.getRequest().getParam('key')) {
      this.set('message', 'ERR_EMPTY_KEY');
      this.getResponse().withStatus(StatusCodes.BAD_REQUEST);
      return;
    }

    // Make sure a body is set
    if(!this.getRequest().getBody()) {
      this.set('message', 'ERR_EMPTY_BODY');
      this.getResponse().withStatus(StatusCodes.BAD_REQUEST);
      return;
    }

    if(!await Database.set(this.getRequest().getParam('key'), JSON.parse(this.getRequest().getBody()))) {
      this.set('message', 'ERR_ADD');
      this.getResponse().withStatus(StatusCodes.INTERNAL_SERVER_ERROR);
      return;
    }

    this.set('message', 'OK');
  }

  public async remove() {
    // We always respond with plaintext
    this.getResponse().withType('text/plain');

    // Make sure we are authenticated
    if(!this.Auth.isAuthorized()) {
      this.set('message', 'ERR_INVALID_BEARER_TOKEN');
      this.getResponse().withStatus(StatusCodes.UNAUTHORIZED);
      return;
    }

    // Make sure a key is set
    if(!this.getRequest().getParam('key')) {
      this.set('message', 'ERR_EMPTY_KEY');
      this.getResponse().withStatus(StatusCodes.BAD_REQUEST);
      return;
    }

    // Remove the key from database
    if(!await Database.delete(this.getRequest().getParam('key'))) {
      this.set('message', 'ERR_DELETE');
      this.getResponse().withStatus(StatusCodes.INTERNAL_SERVER_ERROR);
      return;
    }

    this.set('message', 'OK');
  }
}
