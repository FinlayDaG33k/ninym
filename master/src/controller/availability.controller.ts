import { AppController } from "./app.controller.ts";
import { Database } from "../database.ts";

export class AvailabilityController extends AppController {
  
  public async index() {
    let status = await Database.get('availability-status');
    if(!status) status = 'unknown';

    this.getResponse().withType('text/plain');
    this.getResponse().withHeader('Access-Control-Allow-Origin', '*');
    this.set('message', status);
  }
}
