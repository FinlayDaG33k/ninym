import { AppController } from "./app.controller.ts";
import { Queue, QueueItem } from "../queue.ts";
import {Database} from "../database.ts";

export class MetricsController extends AppController {
  private metrics = '';
  
  public async index() {
    this.metrics = '';
    
    // Add queue metrics
    this.addMetric('ninym_queue_items_waiting', Queue.dump().filter((item: QueueItem) => item.times.startedAt === null && item.times.reportedAt === null).length);
    this.addMetric('ninym_queue_items_running', Queue.dump().filter((item: QueueItem) => item.times.startedAt !== null && item.times.reportedAt === null).length);
    this.addMetric('ninym_queue_items_finished', Queue.dump().filter((item: QueueItem) => item.times.startedAt !== null && item.times.reportedAt !== null).length);
    this.addMetric('ninym_queue_items_with_error', Queue.dump().filter((item: QueueItem) => item.report.errors.length > 0).length);

    // Add UPS metrics
    const ups = await Database.get('ups-state');
    this.addMetric('ninym_ups_max_load{ups="infra"}', ups['maxWatt']);
    this.addMetric('ninym_ups_current_load{ups="infra"}', ups['loadWatt']);
    this.addMetric('ninym_ups_charge{ups="infra"}', ups['chargePercent']);
    this.addMetric('ninym_ups_status{ups="infra"}', ups['status']);
    
    this.getResponse().withType('text/plain');
    this.set('message', this.metrics);
  }
  
  private addMetric(key: string, value: any) {
    this.metrics += `${key} ${value}\n`
  }
}
