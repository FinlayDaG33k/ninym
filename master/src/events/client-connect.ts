import { Database } from "../database.ts";

export class ClientConnectEvent {
  private readonly pushables: string[] = [
    'crypto-tickers',
    'server-statuses',
    'ups-state',
    'ical-schedule',
    'weather',
    'availability-status',
    'rss-feed',
    'fuel-prices',
    'internet-status'
  ];
  
  public async execute(data: any) {
    // Make sure the client is still alive
    if(!data.client) return;
    if(data.client.isClosed) return;

    // Push pushables
    for(const pushable of this.pushables) {
      data.client.send(JSON.stringify({
        event: 'DATABASE_UPDATE',
        data: {
          key: pushable,
          content: await Database.get(pushable)
        }
      }));
    }
  }
}
