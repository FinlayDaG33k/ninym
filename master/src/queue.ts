import { Time, Random, Logger, Loki, Configure } from "../deps.ts";

export interface QueueItem {
  id: string;
  job: string;
  opts: any;
  report: {
    errors: string[];
  };
  times: {
    addedAt: Time;
    startedAt: Time|null;
    reportedAt: Time|null;
  };
}

export class Queue {
  private static items: QueueItem[] = [];
  public static dump(): QueueItem[] { return Queue.items; }

  /**
   * Check whether the queue currently has any items
   *
   * @returns boolean
   */
  public static hasJobs(): boolean {
    const item = Queue.items.find((item: QueueItem) => item.times.startedAt === null && item.times.reportedAt === null);
    return typeof item !== 'undefined';
  }

  /**
   * Find the first QueueItem that hasn't been started or finished.
   * Marks the found QueueItem as "started".
   *
   * @returns QueueItem|null
   */
  public static startItem() {
    const item = Queue.items.find((item: QueueItem) => item.times.startedAt === null && item.times.reportedAt === null);
    if(!item) {
      Logger.debug('Queue is empty!');
      return null;
    }

    Logger.debug(`Starting job "${item.id}"!`);

    // Update the start time
    item.times.startedAt = new Time();

    // Return needed information
    return {
      id: item.id,
      job: item.job,
      opts: item.opts
    };
  }

  public static async reportItem(id: string, errors: string[] = []) {
    const item = Queue.items.find((item: QueueItem) => item.id === id);
    if(!item) return;
    Logger.debug(`Got report for "${id}": ${errors === null ? 'No error!' : errors.length}`);
    item.times.reportedAt = new Time();
    item.report.errors = errors;
    
    // Add all errors to Loki
    if(errors.length > 0) {
      const loki = new Loki(
        Configure.get('loki_host', 'http://localhost:3100'),
        Configure.get('loki_scope','ninym-dev')
      );
      
      let entries: string[][] = [];
      for(const error of errors) {
        entries.push([
          (item.times.reportedAt.time.getTime() * 1_000_000).toString(),
          `${item.job}: ${error}`
        ]);
      }

      await loki.send({
        stream: { application: 'ninym', event: 'error' },
        values: entries,
      });
    }
  }

  public static async add(job: string, opts: any = {}) {
    // Make sure an identical job wasn't already waiting in the queue
    if(Queue.isQueued(job, opts)) {
      Logger.warning('An identical job was already waiting in queue, skipping...');
      return;
    }

    // Create our QueueItem
    const item: QueueItem = {
      id: await Random.string(16),
      job: job,
      opts: opts,
      report: {
        errors: [],
      },
      times: {
        addedAt: new Time(),
        startedAt: null,
        reportedAt: null,
      }
    };

    // Push QueueItem onto the list
    Logger.debug(`Adding job for "${job}" (id: ${item.id})`);
    Queue.items.push(item);
  }

  private static isQueued(job: string, opts: any = {}): Boolean {
    // Find the first job that has the same name and wasn't started yet
    // Return false if none was found
    const queued = Queue.items.find((item: QueueItem) => item.job === job && item.times.startedAt === null && item.times.reportedAt === null);
    if(!queued) return false;

    // Check if the jobs have the same amount of options
    // Return false if not
    if(Object.keys(queued.opts).length === Object.keys(opts).length) return true;

    // Check if every option has the same value
    // Return the status
    //@ts-ignore Don't fucking care about weird index error
    return Object.keys(queued).every((key: string) => opts.hasOwnProperty(key) && opts[key] === queued[key]);
  }

  public static clean(): void {
    // Create a start date from where we'll clean up
    const start = new Date();

    // Create an array filter on our queue
    Queue.items = Queue.items.filter((item: QueueItem) => {
      return new Time(item.times.addedAt.getTime.toISOString()).add('24h').getTime >= start;
    });
  }
}
