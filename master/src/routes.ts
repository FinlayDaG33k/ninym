import { Router } from "../deps.ts";

Router.add({ path: '/', controller: 'Home', action: 'index', method: 'GET' });
Router.add({ path: '/availability', controller: 'Availability', action: 'index', method: 'GET'});
Router.add({ path: '/list', controller: 'Status', action: 'index', method: 'GET' });
Router.add({ path: '/metrics', controller: 'Metrics', action: 'index', method: 'GET'});
Router.add({ path: '/status/:name', controller: 'Status', action: 'update', method: 'PATCH' });
Router.add({ path: '/anime', controller: 'Anidb', action: 'index', method: 'GET'});
Router.add({ path: '/anime/json', controller: 'Anidb', action: 'display', method: 'GET'});
Router.add({ path: '/queue/check', controller: 'Queue', action: 'check', method: 'GET'});
Router.add({ path: '/queue/get', controller: 'Queue', action: 'get', method: 'GET'});
Router.add({ path: '/queue/report', controller: 'Queue', action: 'report', method: 'POST'});
Router.add({ path: '/queue/dump', controller: 'Queue', action: 'dump', method: 'GET'});
Router.add({ path: '/database/get/:key', controller: 'Database', action: 'display', method: 'GET' });
Router.add({ path: '/database/set/:key', controller: 'Database', action: 'add', method: 'PUT' });
Router.add({ path: '/database/remove/:key', controller: 'Database', action: 'remove', method: 'DELETE' });
