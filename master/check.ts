/**
 * This script is intended to check all files in the "src" directory during the build stage.
 * Doing this allows the program to start up significantly faster after deployment.
 */
import { CheckSource } from "./deps.ts";
const checker = new CheckSource('./src');
await checker.run();
