# HTTP Requests

Here, you can find several scripts to test HTTP requests against external services Ninym uses.  
These are just for testing and debugging purposes and aren't used by Ninym herself.  
Originally, I used Insomnia for this but this makes it easier and more consistent.
